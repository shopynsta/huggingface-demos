import os

import torch
import torch_xla.core.xla_model as xm
import torch_xla.distributed.parallel_loader as pl

# import torch_xla.distributed.xla_backend
from datasets import load_dataset
from torch.optim import AdamW
from torch.utils.data import DataLoader
from torch.utils.data.distributed import DistributedSampler
from tqdm.auto import tqdm
from transformers import AutoModelForSequenceClassification, AutoTokenizer

torch.manual_seed(0)

batch_size = 8
num_epochs = 3

device = "xla"

# Initialize distributed training
torch.distributed.init_process_group(device)
world_size = xm.xrt_world_size()

dataset = load_dataset("yelp_review_full")

tokenizer = AutoTokenizer.from_pretrained("bert-base-cased")


def tokenize_function(examples):
    return tokenizer(examples["text"], padding="max_length", truncation=True)


tokenized_datasets = dataset.map(tokenize_function, batched=True)
tokenized_datasets = tokenized_datasets.remove_columns(["text"])
tokenized_datasets = tokenized_datasets.rename_column("label", "labels")
tokenized_datasets.set_format("torch")

small_train_dataset = tokenized_datasets["train"].shuffle(seed=42).select(range(10000))
small_eval_dataset = tokenized_datasets["test"].shuffle(seed=42).select(range(10000))

# Set up distributed data loader
train_sampler = None
if world_size > 1:
    train_sampler = DistributedSampler(
        small_train_dataset,
        num_replicas=world_size,
        rank=xm.get_ordinal(),
        shuffle=True,
    )
train_loader = DataLoader(
    small_train_dataset, batch_size=batch_size, sampler=train_sampler, shuffle=not train_sampler
)

train_device_loader = pl.MpDeviceLoader(train_loader, device)
num_training_steps = num_epochs * len(train_device_loader)
progress_bar = tqdm(range(num_training_steps))

eval_dataloader = DataLoader(small_eval_dataset, batch_size=batch_size)

model = AutoModelForSequenceClassification.from_pretrained("bert-base-cased", num_labels=5)
model.to(device)

optimizer = AdamW(model.parameters(), lr=5e-5)

model.train()
for epoch in range(num_epochs):
    for batch in train_device_loader:
        batch = {k: v.to(device) for k, v in batch.items()}
        outputs = model(**batch)
        optimizer.zero_grad()
        loss = outputs.loss
        loss.backward()
        xm.optimizer_step(optimizer)
        progress_bar.update(1)
    print(
        "Epoch {}, rank {}, Loss {:0.4f}".format(epoch, xm.get_ordinal(), loss.detach().to("cpu"))
    )

os.makedirs("checkpoints", exist_ok=True)
checkpoint = {"state_dict": model.state_dict()}
xm.save(checkpoint, "checkpoints/checkpoint.pt")
