{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Evaluating models\n",
    "\n",
    "In this notebook, we're going to evaluate models with the Hugging Face [Evaluate](https://github.com/huggingface/evaluate) library.\n",
    "\n",
    "```evaluate``` documentation: https://huggingface.co/docs/evaluate/index"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1 - Setup"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "%%sh\n",
    "pip -q install transformers datasets evaluate --upgrade"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 2 - Load the model and the evaluation dataset"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import transformers\n",
    "from transformers import AutoModelForSequenceClassification, AutoTokenizer\n",
    "\n",
    "print(transformers.__version__)\n",
    "\n",
    "model = AutoModelForSequenceClassification.from_pretrained(\n",
    "    \"juliensimon/distilbert-amazon-shoe-reviews\"\n",
    ")\n",
    "tokenizer = AutoTokenizer.from_pretrained(\"juliensimon/distilbert-amazon-shoe-reviews\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import datasets\n",
    "from datasets import load_from_disk\n",
    "\n",
    "print(datasets.__version__)\n",
    "\n",
    "eval_dataset = load_from_disk(\"./data/test\")\n",
    "eval_dataset"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 3 - Set up the evaluation job"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import evaluate\n",
    "from evaluate import evaluator\n",
    "\n",
    "print(evaluate.__version__)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evaluate supports plenty of [built-in metrics](https://github.com/huggingface/evaluate/tree/main/metrics), and you can add your own too."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evaluate.list_evaluation_modules(\"metric\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's score the accuracy of the model on our evaluation dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metric = evaluate.load(\"accuracy\")\n",
    "print(metric.description)\n",
    "print(metric.citation)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eval = evaluator(\"text-classification\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "label_column = \"labels\"\n",
    "label_mapping = model.config.label2id\n",
    "# {\"LABEL_0\": 0.0, \"LABEL_1\": 1.0, \"LABEL_2\": 2.0, \"LABEL_3\": 3.0, \"LABEL_4\": 4.0}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = eval.compute(\n",
    "    model_or_pipeline=model,\n",
    "    tokenizer=tokenizer,\n",
    "    data=eval_dataset,\n",
    "    metric=metric,\n",
    "    label_column=label_column,\n",
    "    label_mapping=label_mapping,\n",
    ")\n",
    "results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 4 - Share evaluation results on the hub"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can push evaluation results to the model page."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "evaluate.push_to_hub(\n",
    "    model_id=\"juliensimon/distilbert-amazon-shoe-reviews\",  # model repository on hub\n",
    "    metric_value=results[\"accuracy\"],  # metric value\n",
    "    metric_type=\"accuracy\",  # metric name, e.g. accuracy.name\n",
    "    metric_name=\"Accuracy\",  # pretty name which is displayed\n",
    "    dataset_type=\"amazon_us_reviews\",  # dataset name on the hub\n",
    "    dataset_name=\"Amazon US reviews\",  # pretty name\n",
    "    dataset_split=\"Shoes\",  # dataset split used\n",
    "    task_type=\"text-classification\",\n",
    "    task_name=\"Text Classification\",  # pretty name for task\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Evaluation on model page](images/07.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 5 - Trying other metrics"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here's the [Mean Square Error](https://github.com/huggingface/evaluate/tree/main/metrics/mse) metric."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "metric = evaluate.load(\"mse\")\n",
    "print(metric.description)\n",
    "print(metric.citation)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = eval.compute(\n",
    "    model_or_pipeline=model,\n",
    "    tokenizer=tokenizer,\n",
    "    data=eval_dataset,\n",
    "    metric=metric,\n",
    "    label_column=label_column,\n",
    "    label_mapping=label_mapping,\n",
    ")\n",
    "results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "instance_type": "ml.t3.medium",
  "kernelspec": {
   "display_name": "conda_pytorch_p38",
   "language": "python",
   "name": "conda_pytorch_p38"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
