import subprocess
import sys

input_dir = "/opt/ml/processing/input"
output_dir = "/opt/ml/processing/output"

req_dir = f"{input_dir}/requirements"
inference_dir = f"{input_dir}/deploy"
model_dir = f"{output_dir}/compiled_model"
artifact_dir = f"{output_dir}/model_artifact"

subprocess.check_call(
    [
        sys.executable,
        "-m",
        "pip",
        "install",
        "-r",
        f"{req_dir}/requirements.txt",
    ]
)

import argparse
import os
import tarfile

import torch
import torch.neuron
from transformers import AutoConfig, AutoModelForSequenceClassification, AutoTokenizer

os.environ["TOKENIZERS_PARALLELISM"] = "false"

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--model-name", type=str, default="bert-base-cased")
    parser.add_argument("--num-labels", type=int, default=5)
    parser.add_argument("--num-neuron-cores", type=int)

    args, _ = parser.parse_known_args()
    print("Received arguments {}".format(args))

    # Extract model file
    with tarfile.open(f"{input_dir}/model.tar.gz") as tar:
        tar.extractall(f"{input_dir}")

    # Load checkpoint
    tokenizer = AutoTokenizer.from_pretrained(args.model_name)
    config = AutoConfig.from_pretrained(args.model_name)
    config.num_labels = args.num_labels
    model = AutoModelForSequenceClassification.from_config(config=config)
    checkpoint = torch.load(f"{input_dir}/checkpoint.pt")
    model.load_state_dict(checkpoint["state_dict"])
    print(model)

    # Sample input
    positive_example = "This is a really great restaurant, I loved it"
    positive = tokenizer(positive_example, return_tensors="pt")

    # Convert sample input to a format that is compatible with TorchScript tracing:
    # Only Tensors and (possibly nested) Lists, Dicts, and Tuples of Tensors can be traced
    positive_input = (
        positive["input_ids"],
        positive["attention_mask"],
        positive["token_type_ids"],
    )

    # Convert model with Neuron
    neuron_model = torch.neuron.trace(
        model,
        positive_input,
        strict=False,
        compiler_args=["--neuroncore-pipeline-cores", str(args.num_neuron_cores)],
    )
    print("Model compiled.")

    # Save model artifact
    tokenizer.save_pretrained(model_dir)
    config.save_pretrained(model_dir)
    neuron_model.save(f"{model_dir}/compiled_model.pt")
    print("Model saved.")

    with tarfile.open(f"{artifact_dir}/model.tar.gz", "w:gz") as tar:
        tar.add(model_dir, arcname=".")
        tar.add(inference_dir, arcname=".")
    print("Model artifact saved.")
